package utc.fr.AI13_Projet.service;

        import utc.fr.AI13_Projet.models.Quizz;

        import java.util.List;
        import java.util.Optional;
public interface IQuizzService {

    List<Quizz> getAllQuizzes();

    Optional<Quizz> getQuizzById(long id);

    Quizz addQuizz(Quizz quizz);

    String deleteQuizz(long id);

    String deleteQuizz(Quizz quizz);  
    //all active quizzes
  	List<Quizz> findByIsActive(boolean isActive);
  	//all quizzes created by a specific user(admin)
  	List<Quizz> findByCreatorId(Long creatorId);
  	//all quizzes accessible by a specific user(stagiaire)
  	List<Quizz> findByEnlistedUsersId(long enlistedUserId);

	void updateQuizz(Quizz quizz);

	boolean existsBySujet(String sujet);

}
