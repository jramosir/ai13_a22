package utc.fr.AI13_Projet.service;

import utc.fr.AI13_Projet.models.Answer;

import java.util.List;
import java.util.Optional;

public interface IAnswerService {

    public List<Answer> findAllAnswers();
    //public List<Answer> findAnswerByQuizzId(long id);

    void deleteAnswer(Answer answer);
    Optional<Answer> findAnswerById(long id);

    public Answer saveAnswer(Answer answer);

    Optional<Answer> findAnswerbyId(long id);

}
