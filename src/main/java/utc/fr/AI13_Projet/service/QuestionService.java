package utc.fr.AI13_Projet.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import utc.fr.AI13_Projet.models.Question;
import utc.fr.AI13_Projet.dao.QuestionRepository;

@Service
public class QuestionService implements IQuestionService{

	@Autowired
	QuestionRepository questionRepository;
	
	@Override
	public List<Question> getAllQuestions() {
		// TODO Auto-generated method stub
		return questionRepository.findAll();
	}

	@Override
	public Optional<Question> findQuestionById(long id) {
		return questionRepository.findById(id);
	}

	@Override
	public Question addQuestion(Question quest) {
		return questionRepository.save(quest);
	}

	@Override
	public void deleteQuestion(long id) {
		questionRepository.deleteById(id);
		
	}

	@Override
	public void deleteQuestion(Question question) {
		questionRepository.delete(question);
	}

	@Override
	public Question updateQuestion(Question question) {
		return questionRepository.save(question);
	}
}
