package utc.fr.AI13_Projet.service;
import utc.fr.AI13_Projet.models.User;
import java.util.List;
import java.util.Optional;

public interface IUserService {

    List<User> getAllUsers();

    Optional<User> getUserById(long id);

    User addUser(User user);

    String deleteUser(long id);

    String deleteUser(User user);

    void updateUser(User user);
    
    boolean existsById(long id);

	boolean existsByMail(String mail);
	
	User getConnectedUser();
}

