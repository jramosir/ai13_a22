package utc.fr.AI13_Projet.service;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import utc.fr.AI13_Projet.dao.UserRepository;
import utc.fr.AI13_Projet.models.User;
import utc.fr.AI13_Projet.security.services.UserDetailsImpl;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements IUserService{

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAllUsers(){
        return userRepository.findAll();
    }

    @Override
    public Optional<User> getUserById(long id){
        return userRepository.findById(id);

    }

    @Override
    public User addUser(User user){
        return userRepository.save(user);
    }

    @Override
    public String deleteUser(long id){
        userRepository.deleteById(id);
        return "The user has been deleted";
    }

    @Override
    public String deleteUser(User user){
        userRepository.delete(user);
        return "The user has been deleted";
    }

    @Override
    public void updateUser(User user) {
        if(user != null)
        {
            userRepository.save(user);
        }
    }
    
    @Override
    public boolean existsById(long id) {
    	return userRepository.existsById(id);
    }

	@Override
	public boolean existsByMail(String mail) {
		return userRepository.existsByMail(mail);
	}

	@Override
	public User getConnectedUser() {
		//Get authenticated user info to put into quizz.creator
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		User user = userDetails.getApplicationUser();
		return user;
	}
}
