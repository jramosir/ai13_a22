package utc.fr.AI13_Projet.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import utc.fr.AI13_Projet.dao.QuizzRepository;
import utc.fr.AI13_Projet.models.Quizz;

import java.util.List;
import java.util.Optional;

@Service
public class QuizzService implements IQuizzService{

    @Autowired
    QuizzRepository quizzRepository;


    @Override
    public List<Quizz> getAllQuizzes(){ return quizzRepository.findAll(); }

    @Override
    public Optional<Quizz> getQuizzById(long id){
        return quizzRepository.findById(id);
    }

    @Override
    public Quizz addQuizz(Quizz quizz){
        return quizzRepository.save(quizz);
    }

    @Override
    public String deleteQuizz(long id){
        quizzRepository.deleteById(id);
        return "The quizz has been deleted";
    }

    @Override
    public String deleteQuizz(Quizz quizz){
        quizzRepository.delete(quizz);
        return "The quizz has been deleted";
    }

	@Override
	public List<Quizz> findByIsActive(boolean isActive) {
		return quizzRepository.findByIsActive(isActive);
	}

	@Override
	public List<Quizz> findByCreatorId(Long creatorId) {
		return quizzRepository.findByCreatorId(creatorId);
	}

	@Override
	public List<Quizz> findByEnlistedUsersId(long enlistedUserId) {
		return quizzRepository.findByEnlistedUsersId(enlistedUserId);
	}

	@Override
	public void updateQuizz(Quizz quizz) {
		if(quizz != null)
        {
            quizzRepository.save(quizz);
        }
	}
	
	@Override
	public boolean existsBySujet(String sujet) {
		return quizzRepository.existsBySujet(sujet);
	}

}