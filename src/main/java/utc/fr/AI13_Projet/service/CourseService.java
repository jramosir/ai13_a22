package utc.fr.AI13_Projet.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import utc.fr.AI13_Projet.dao.CourseRepository;
import utc.fr.AI13_Projet.models.Course;

@Service
public class CourseService implements ICourseService{
	
	@Autowired
	private CourseRepository courseRepository;
	
	public Optional<Course> findCoursebyId(long id){
		return courseRepository.findById(id);
	}
	
	public List<Course> getAllCourses(){
	return courseRepository.findAll();
    }
    
	public Course addCourse(Course course) {
    	return courseRepository.save(course);
    }


	public void deleteCoursebyId(long id) {
    	courseRepository.deleteById(id);
    }
    
	public void deleteCourse(Course course) {
    	courseRepository.delete(course);
    }

	@Override
	public Course updateCourse(Long id, Course course) {
		return courseRepository.findById(id)
				.map(u -> {
                    u.setDate(course.getDate());
                    u.setDuration(course.getDuration());
                    u.setScore(course.getScore());
                     return courseRepository.save(u);
                 }).orElseThrow(() -> new RuntimeException("Course not found"));
	}

	@Override
	public List<Course> findByQuizzId(Long id) {
		return courseRepository.findByQuizzId(id);
	}



}
