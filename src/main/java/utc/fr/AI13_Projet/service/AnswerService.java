package utc.fr.AI13_Projet.service;

import org.springframework.stereotype.Service;
import utc.fr.AI13_Projet.dao.AnswerRepository;
import utc.fr.AI13_Projet.models.Answer;

import java.util.List;
import java.util.Optional;

@Service
public class AnswerService implements IAnswerService{

    private AnswerRepository answerRepository;

    public AnswerService(AnswerRepository answerRepository) {
        this.answerRepository = answerRepository;
    }

    @Override
    public List<Answer> findAllAnswers(){
        return (List<Answer>) answerRepository.findAll();
    }

    /*@Override
    public List<Answer> findAnswerByQuizzId(long id){
        return (List<Answer>) answerRepository.findByQuizzId();
    }*/

    @Override
    public Optional<Answer> findAnswerById(long id){
        Optional<Answer> answer = answerRepository.findById(id);
        return answer;

    }

    @Override
    public Answer saveAnswer(Answer answer){
        return answerRepository.save(answer);
    }

    @Override
    public Optional<Answer> findAnswerbyId(long id) {
        return Optional.empty();
    }

    @Override
    public void deleteAnswer(Answer answer)
    {
        answerRepository.delete(answer);
    }

}
