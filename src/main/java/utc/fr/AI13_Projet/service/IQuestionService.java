package utc.fr.AI13_Projet.service;

import utc.fr.AI13_Projet.models.Question;
import utc.fr.AI13_Projet.models.User;

import java.util.List;
import java.util.Optional;

public interface IQuestionService {
    List<Question> getAllQuestions();

    Optional<Question> findQuestionById(long id);

    Question addQuestion(Question quest);

    void deleteQuestion(long id);

    void deleteQuestion(Question question);

    Question updateQuestion(Question question);
    
    
}
