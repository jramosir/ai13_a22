package utc.fr.AI13_Projet.service;

import utc.fr.AI13_Projet.models.Course;
import utc.fr.AI13_Projet.models.User;

import java.util.List;
import java.util.Optional;

public interface ICourseService {
	
    Optional<Course> findCoursebyId(long id);

    List<Course> getAllCourses();
    
    Course addCourse(Course course);

    void deleteCoursebyId(long id);

    void deleteCourse(Course course);

    Course updateCourse(Long id, Course user);
    
    List<Course> findByQuizzId(Long id);
}
