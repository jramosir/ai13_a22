package utc.fr.AI13_Projet.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import utc.fr.AI13_Projet.models.User;
import utc.fr.AI13_Projet.dao.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	UserRepository userRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(String mail) throws UsernameNotFoundException {
		User user = userRepository.findByMail(mail)
				.orElseThrow(() -> new UsernameNotFoundException("User Not Found: " + mail));
		if(user.isActive() == false) {
			throw new DisabledException("User "+ mail +" is inactive");
		}
		return UserDetailsImpl.build(user);
	}

}
