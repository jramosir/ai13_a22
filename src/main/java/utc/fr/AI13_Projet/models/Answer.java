package utc.fr.AI13_Projet.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonBackReference;

import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "ai13_answers")
public class Answer {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String libelle;
	private int ordre;
	
	@ManyToOne
	@com.fasterxml.jackson.annotation.JsonIgnore
	Question question;

	
	@ManyToMany(mappedBy = "answers", fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	@JsonIgnore
	Set<Course> courses;

	/*
	@OneToOne(mappedBy = "correctAnswer")
	@JsonIgnore
    private Question answerTo;
	*/
	
	public Answer(){

	}

	
	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public Long getAnswerId(){
		return id;
	}
	public void setAnswerId(Long id){
		this.id = id;
	}

	public String getLibelle(){
		return libelle;
	}
	public void setLibelle (String libelle){
		this.libelle = libelle;
	}
	
	public Long getId() {
		return id;
	}
	public int getOrdre() {
		return ordre;
	}

	public void setOrdre(int ordre) {
		this.ordre = ordre;
	}
	
	public Set<Course> getCourses() {
		return courses;
	}
	
	/*public Question getAnswerTo() {
		return answerTo;
	}*/

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append( "Answer:" ).append( this.id).append( " > " ).append( this.libelle );
		return builder.toString();
	}



}
