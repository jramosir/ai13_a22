package utc.fr.AI13_Projet.models;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.core.annotation.Order;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Entity
@Table(name = "ai13_questions")
public class Question {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String libelle;
	
	//list of quizzes linked to the question
	@ManyToMany(mappedBy="quizzQuestions", fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	@JsonIgnore
	private Collection<Quizz> quizzes;
	
	@OneToMany(mappedBy = "question", fetch = FetchType.EAGER)
	@OrderBy("ordre ASC")
	private Collection<Answer> answers;

	@OneToOne
    @JoinColumn(name = "answerId", referencedColumnName = "id")
	private Answer correctAnswer;

	public Answer getCorrectAnswer() {
		return correctAnswer;
	}

	public void setCorrectAnswer(Answer correctAnswer) {
		this.correctAnswer = correctAnswer;
	}
	
	@Column(columnDefinition = "boolean default true")
	private boolean active = true;

	public Collection<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(Collection<Answer> answers) {
		this.answers = answers;
	}



	public Question(){
	
	}

	public Long getId(){
		return id;
	}
	public void setId (Long id){
		this.id = id;
	}

	public String getLibelle(){
		return libelle;
	}
	public void setLibelle (String libelle){
		this.libelle = libelle;
	}

	public boolean isActive(){
		return active;
	}
	public void setActive (boolean active){
		this.active = active;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append( "Question:" ).append( this.id )
        .append( " > " ).append( this.libelle ).append( ">" );
		return builder.toString();
	}


	public Collection<Quizz> getQuizzes() {
		return quizzes;
	}

	public void setQuizzes(Collection<Quizz> quizzes) {
		this.quizzes = quizzes;
	}

}