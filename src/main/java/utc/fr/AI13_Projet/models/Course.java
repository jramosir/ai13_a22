package utc.fr.AI13_Projet.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "ai13_parcours")
public class Course {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private Date date;

	public Long getId() {
		return id;
	}

	public Course() {
	}

	public LocalDateTime getUpdateTimestamp() {
		return updateTimestamp;
	}

	public LocalDateTime getCreationTimestamp() {
		return creationTimestamp;
	}

	public Quizz getQuizz() {
		return quizz;
	}

	public Set<Answer> getAnswers() {
		return answers;
	}

	public User getUser() {
		return user;
	}

	private int score;

    private int duration;

    @UpdateTimestamp
    private LocalDateTime updateTimestamp;

    @CreationTimestamp
    private LocalDateTime creationTimestamp;

    @ManyToOne
    @JoinColumn(name = "quizzId")
    private Quizz quizz;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "ai13_parcours_answers",
			joinColumns = @JoinColumn(name = "parcours_id"),
			inverseJoinColumns = @JoinColumn(name = "answer_id"))
	Set<Answer> answers;

	public void setUser(User user) {
		this.user = user;
	}

	@ManyToOne
    @JoinColumn(name = "userId")
	@JsonIgnore
    private User user;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

}
