package utc.fr.AI13_Projet.models;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "ai13_quizzes")
public class Quizz {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column
	private Long id;

	private String sujet;

	@Column(columnDefinition = "boolean default true")
	private boolean isActive = true;

	@CreationTimestamp
	private LocalDateTime creationDate;

	@ManyToOne
	@JoinColumn(name = "creatorId", insertable = false, updatable = false)
	@JsonIgnore
	private User creator;

	//list of users that have access to the quizz
	@ManyToMany(mappedBy="accessibleQuizzes", fetch = FetchType.EAGER)
	@JsonIgnore
	private Collection<User> enlistedUsers;
	
	//list of questions linked to the quizz
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@Fetch(value = FetchMode.SUBSELECT)
	@JoinTable(
			  name = "ai13_quizz_questions",
			  joinColumns = @JoinColumn(name="quizzId",referencedColumnName="id"),
			  inverseJoinColumns = @JoinColumn(name="questionId",referencedColumnName="id"))
    private Collection<Question> quizzQuestions;

	//chained list for quizz versions
	/*
	@ManyToOne
	@JoinColumn(name="quizzId",referencedColumnName="id", insertable = false, updatable = false)
	private Quizz previousQuizzVersion;
	*/
	public Quizz(){

	}

	public Long getId(){
		return id;
	}
	public void setId (Long id){
		this.id = id;
	}

	public String getSujet(){
		return sujet;
	}
	public void setSujet (String sujet){
		this.sujet = sujet;
	}

	public LocalDateTime getCreationDate(){
		return creationDate;
	}

	public boolean isActive(){
		return isActive;
	}
	public void setActive (boolean active){
		this.isActive = active;
	}

	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append( "Questionnaire:" ).append( this.id )
		.append( " > " ).append( this.sujet ).append( ">" )
		.append(this.creationDate.toString());
		return builder.toString();
	}

	public Collection<User> getEnlistedUsers() {
		return enlistedUsers;
	}
	public Collection<Question> getQuizzQuestions() {
		return quizzQuestions;
	}
	public void addQuestion(Question question) {
		question.getQuizzes().add(this);
	    this.quizzQuestions.add(question);
	  }
	
	/*
	public Quizz getPreviousQuizzVersion() {
		return previousQuizzVersion;
	}
	*/
	public void setEnlistedUsers(Collection<User> enlistedUsers) {
		this.enlistedUsers = enlistedUsers;
	}
}
