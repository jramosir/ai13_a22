package utc.fr.AI13_Projet.models;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import utc.fr.AI13_Projet.dto.UserDTO;
import org.modelmapper.ModelMapper;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * @author Santiago RAMOS
 * This class is a spring entity used to map a user class object with a user database table in order to implement CRUD methods efficiently
 * ATTENTION: Naming convention in important while using this god forsaken framework otherwise repository methods don't work........
 */

@Entity
@Table(name = "ai13_users",
	uniqueConstraints = {
	    @UniqueConstraint(columnNames = "mail")
	})
public class User {
	/*to generate increasing values for new users id
	 *link to database column is automatically resolved if the attribute name is the same
	 *otherwise it must be declared
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	private String firstName;
	private String lastName;
	@NotBlank
	private String mail;
	@NotBlank
	private String password;
	private String gender;
	private String company;
	private String telephone;

	@CreationTimestamp
    private LocalDateTime creationDate;
 
    @UpdateTimestamp
    private LocalDateTime modificationDate;
	
    //columnDefinition only acts at table creation, not for object insertion.
	@Column(columnDefinition = "boolean default true")
	private boolean active = true;
	@Column(columnDefinition = "boolean default false")
	private boolean admin = false;

	//quizzes accessible to the user (stagiare)
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(
			  name = "ai13_user_quizzes",
			  joinColumns = @JoinColumn(name="userId",referencedColumnName="id"),
			  inverseJoinColumns = @JoinColumn(name="quizzId",referencedColumnName="id"))
    private Collection<Quizz> accessibleQuizzes;

	
	//quizzes created by an admin user
	@OneToMany(mappedBy = "creator")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Set<Quizz> quizzesCreated;
	
	//list of quizzes completed by the user (course - parcours)
	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@OrderBy("score DESC")
	private Set<Course> courseDoneList;
	
	
	public User(){
		//Default constructor with no argument.
		//no overloaded constructor because all values are not null in the table, therefore all values must be filled anyways.
	}
	/*
	 * Setter and Getter for each class attribute
	 */
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime date) {
		this.creationDate = date;
	}
	public LocalDateTime getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(LocalDateTime date) {
		this.modificationDate = date;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	
	public Set<Course> getCourseDoneList() {
		return courseDoneList;
	}

	public Set<Quizz> getQuizzesCreated() {
		return quizzesCreated;
	}
	public Collection<Quizz> getAccessibleQuizzes() {
		return accessibleQuizzes;
	}
	public void setAccessibleQuizzes(Collection<Quizz> accessibleQuizzes) {
		this.accessibleQuizzes = accessibleQuizzes;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append( "User:" ).append( this.id )
		.append( " > " ).append( this.firstName ).append( "." )
		.append(this.lastName);
		return builder.toString();
	}

	public void addQuizz(Quizz quizz) {
		quizz.getEnlistedUsers().add(this);
		this.accessibleQuizzes.add(quizz);
	}

	public void removeQuizz(long quizzId) {
		Quizz quizz = this.accessibleQuizzes.stream().filter(q -> q.getId() == quizzId).findFirst().orElse(null);
		if (quizz != null) {
			this.accessibleQuizzes.remove(quizz);
			quizz.getEnlistedUsers().remove(this);
		}
	}


}
