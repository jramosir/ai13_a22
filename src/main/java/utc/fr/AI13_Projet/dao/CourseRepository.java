package utc.fr.AI13_Projet.dao;

import java.util.List;
import java.util.Optional;

import utc.fr.AI13_Projet.models.Course;
import utc.fr.AI13_Projet.models.Quizz;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface CourseRepository extends JpaRepository<Course, Long>{
	@Query(value="select c from Course c where c.quizz.id = :id")
	List<Course> findByQuizzId(Long id);
}

