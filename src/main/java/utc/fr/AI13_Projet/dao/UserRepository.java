package utc.fr.AI13_Projet.dao;
import java.util.List;
import java.util.Optional;

import utc.fr.AI13_Projet.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface UserRepository extends JpaRepository<User, Long>{
    User getUserByMail(String s);
    Optional<User> findByMail(String mail);
    Boolean existsByMail(String mail);
    
    //@Query("SELECT u FROM User u JOIN FETCH u.accessibleQuizzes WHERE u.id = :id")
    //public Optional<User> findByIdAndFetchAccessibleQuizzesEagerly(@Param("id") Long id);
}
