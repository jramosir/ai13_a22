package utc.fr.AI13_Projet.dao;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import utc.fr.AI13_Projet.models.Quizz;
import org.springframework.data.repository.query.Param;


public interface QuizzRepository extends JpaRepository<Quizz, Long> {
	//all active quizzes
	List<Quizz> findByIsActive(boolean isActive);
	//all quizzes created by a specific user(admin)
	List<Quizz> findByCreatorId(Long creatorId);
	//all quizzes accessible by a specific user(stagiaire)
	List<Quizz> findByEnlistedUsersId(long enlistedUserId);
	Boolean existsBySujet(String sujet);
}