package utc.fr.AI13_Projet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import utc.fr.AI13_Projet.models.Answer;


public interface AnswerRepository extends JpaRepository<Answer, Long>{


    //Object findByQuizzId();
}
