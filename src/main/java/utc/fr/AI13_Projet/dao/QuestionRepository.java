package utc.fr.AI13_Projet.dao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import utc.fr.AI13_Projet.models.Question;

import java.util.List;

public interface QuestionRepository extends JpaRepository<Question, Long>{
}
