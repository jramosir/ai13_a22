package utc.fr.AI13_Projet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import utc.fr.AI13_Projet.dto.CourseDTO;
import utc.fr.AI13_Projet.dto.CourseSimpleDTO;
import utc.fr.AI13_Projet.models.Answer;
import utc.fr.AI13_Projet.models.Course;
import utc.fr.AI13_Projet.models.User;
import utc.fr.AI13_Projet.service.IAnswerService;
import utc.fr.AI13_Projet.service.ICourseService;
import utc.fr.AI13_Projet.service.IUserService;

import java.lang.reflect.Array;
import java.util.*;

@RestController
@RequestMapping("/api")
public class CourseController {

    @Autowired
    IUserService iUserService;
    private final ICourseService iCourseService;

    public CourseController(ICourseService iCourseService) {
        this.iCourseService = iCourseService;
    }

    @GetMapping("courses")
    public List<Course> getAllCourses() {
        return iCourseService.getAllCourses();
    }

    @GetMapping("course/{id}")
    public ResponseEntity<CourseDTO> findCourseId(@PathVariable(value = "id") long id) {
        Optional<Course> course = iCourseService.findCoursebyId(id);
        CourseDTO courseDTO = new CourseDTO();
        courseDTO = courseDTO.convertToCourseDTO(course);
        if(course.isPresent()) {
            return ResponseEntity.ok().body(courseDTO);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @PostMapping("course")
    public Course saveCourse(@Validated @RequestBody Course course) {
        course.setUser(iUserService.getConnectedUser());
        return iCourseService.addCourse(course);
    }
    @GetMapping("user/course")
    public ArrayList<CourseSimpleDTO> getUserCourses(){
        User user = iUserService.getConnectedUser();
        ArrayList<CourseSimpleDTO> result = new ArrayList<>();
        for(Course course : user.getCourseDoneList())
        {
            CourseSimpleDTO courseSimpleDTO = new CourseSimpleDTO();
            courseSimpleDTO = courseSimpleDTO.convertToCourseDTO(course);
            result.add(courseSimpleDTO);
        }
        return result;
    }

}
