package utc.fr.AI13_Projet.controller;

import com.sun.istack.NotNull;

import jdk.swing.interop.SwingInterOpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import utc.fr.AI13_Projet.models.*;
import utc.fr.AI13_Projet.service.IAnswerService;
import utc.fr.AI13_Projet.service.ICourseService;
import utc.fr.AI13_Projet.service.IQuestionService;
import utc.fr.AI13_Projet.service.IQuizzService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/")
public class QuestionController {
	
	@Autowired
	IQuizzService iQuizzService;

    @Autowired
    IAnswerService iAnswerService;
    private final IQuestionService iQuestionService;

    public QuestionController(IQuestionService iQuestionService) {
        this.iQuestionService = iQuestionService;
    }


    @GetMapping("questions")
    public ResponseEntity<Collection<Question>> getAllQuestions() {
    	try {
			//TODO: modifier pour envoyer list de QuizzDTO
			Collection<Question> questionList = iQuestionService.getAllQuestions();

			if (questionList.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(questionList, HttpStatus.OK);
		} catch (Exception e) {
            e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
        
    }

    @GetMapping("question/{id}")
    public ResponseEntity<Question> findQuestionId(@PathVariable(value = "id") Long id) {
        Optional<Question> question = iQuestionService.findQuestionById(id);
        if(question != null && question.isPresent()) {
            return ResponseEntity.ok().body(question.get());
        } else {
        	return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
    @PostMapping("question")
    public ResponseEntity addQuestion(@Validated @RequestBody Question quest) {
        Question addedQuestion = iQuestionService.addQuestion(quest);
        if(quest.getAnswers() != null)
        {
            for(Answer answer : quest.getAnswers())
            {
                answer.setQuestion(addedQuestion);
                iAnswerService.saveAnswer(answer);
            }
            addedQuestion.setCorrectAnswer(quest.getAnswers().stream().findFirst().get());
            iQuestionService.updateQuestion(addedQuestion);
        }

        return new ResponseEntity(HttpStatus.CREATED);
    }

    @DeleteMapping("question/{id}")
    public ResponseEntity deleteQuestion(@Validated @NotNull @PathVariable(value = "id") Long id){
        if(id != null) {
            iQuestionService.deleteQuestion(id);
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }
    
    @PostMapping("/question/{questionId}/quizz/{quizzId}")
	public ResponseEntity<?> addQuestionToQuizz(@PathVariable(value = "questionId") Long questionId, @PathVariable(value = "quizzId") Long quizzId) {
		try {
			Optional<Quizz> quizz = iQuizzService.getQuizzById(quizzId);
			if (quizz != null && quizz.isPresent()) {
				Optional<Question> question = iQuestionService.findQuestionById(questionId);
				quizz.get().addQuestion(question.get());
				iQuizzService.updateQuizz(quizz.get());
			    return new ResponseEntity<>(HttpStatus.OK);
			}
			throw new ResourceNotFoundException("Not found Quizz with id = " + quizzId);
		} catch (Exception e) {
			System.out.print(e);
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
	}
}
