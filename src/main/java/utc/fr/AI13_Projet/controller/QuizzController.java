package utc.fr.AI13_Projet.controller;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import utc.fr.AI13_Projet.dto.QuizzCreateDTO;
import utc.fr.AI13_Projet.dto.QuizzStatsDTO;
import utc.fr.AI13_Projet.dto.UserDTO;
import utc.fr.AI13_Projet.dto.QuizzDTO;
import utc.fr.AI13_Projet.models.Course;
import utc.fr.AI13_Projet.models.Question;
import utc.fr.AI13_Projet.models.Quizz;
import utc.fr.AI13_Projet.models.User;
import utc.fr.AI13_Projet.security.services.UserDetailsImpl;
import utc.fr.AI13_Projet.service.ICourseService;
import utc.fr.AI13_Projet.service.IQuestionService;
import utc.fr.AI13_Projet.service.IQuizzService;
import utc.fr.AI13_Projet.service.IUserService;

import javax.swing.text.html.Option;
import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/api/")
public class QuizzController {
	
	@Autowired
	AuthenticationManager authenticationManager;
	@Autowired
	IQuizzService iQuizzService;
	@Autowired
	IQuestionService iQuestionService;
	@Autowired
	IUserService iUserService;
	@Autowired
	ICourseService iCourseService;
	
	
	@GetMapping("quizz")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<QuizzDTO>> getAllQuizzes() {
		try {
			List<Quizz> quizzList = iQuizzService.getAllQuizzes();
			if (quizzList.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			List<QuizzDTO> quizzDTOList = new ArrayList<QuizzDTO>();
			for (Quizz quizz: quizzList ) {
				QuizzDTO quizzD = new QuizzDTO();
				quizzD = quizzD.convertToUserDTO(quizz);
		        quizzDTOList.add(quizzD);
		      }
			return new ResponseEntity<>(quizzDTOList, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("quizz")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<?> createQuizz(@RequestBody QuizzCreateDTO quizzRequest) {
		try {
			User user = iUserService.getConnectedUser();
			if (iQuizzService.existsBySujet(quizzRequest.getSujet())) {
				return ResponseEntity.badRequest().body("Error: Quizz sujet is already in use!");
			}
			Quizz newquizz = quizzRequest.convertToEntity();
			newquizz.setCreator(user);
			newquizz.setActive(true);
			iQuizzService.addQuizz(newquizz);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/user/quizz")
	public ResponseEntity<List<Quizz>> getAllQuizzesByConnectedUser() {
		try {
			User user = iUserService.getConnectedUser();
			List<Quizz> accessibleQuizzes = (List<Quizz>) user.getAccessibleQuizzes();
			if (accessibleQuizzes.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			List<QuizzDTO> quizzDTOList = new ArrayList<QuizzDTO>();
			for (Quizz quizz: accessibleQuizzes ) {
				QuizzDTO quizzD = new QuizzDTO();
				quizzD = quizzD.convertToUserDTO(quizz);
		        quizzDTOList.add(quizzD);
		      }
			return new ResponseEntity<>(accessibleQuizzes, HttpStatus.OK);
		} catch(Exception e) {
			System.out.print(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@GetMapping("/quizz/{quizzId}")
	public ResponseEntity<Quizz> getQuizzById(@PathVariable(value = "quizzId") Long quizzId){
		try
		{
			Optional<Quizz> quizz = iQuizzService.getQuizzById(quizzId);
			if(quizz.isPresent())
			{
				return new ResponseEntity<>(quizz.get(), HttpStatus.OK);
			}
			return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
		}
		catch(Exception e) {
			System.out.print(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PostMapping("/user/{userId}/quizz/{quizzId}")
	public ResponseEntity<?> enrollUserToQuizz(@PathVariable(value = "userId") Long userId, @PathVariable(value = "quizzId") Long quizzId) {
		try {
			Optional<User> user = iUserService.getUserById(userId);
			if (user != null && user.isPresent()) {
				Optional<Quizz> quizz = iQuizzService.getQuizzById(quizzId);
				if (quizz != null && quizz.isPresent()) {
					user.get().addQuizz(quizz.get());
					iUserService.updateUser(user.get());
				    return new ResponseEntity<>(HttpStatus.OK);
				}
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			System.out.print(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PutMapping("/quizz/{quizzId}/question/{questionId}")
	public ResponseEntity<?> addQuestionToQuizz(@PathVariable(value = "quizzId") Long quizzId, @PathVariable(value = "questionId") Long questionId)
	{
		try {
			Optional<Question> question = iQuestionService.findQuestionById(questionId);
			if (question != null && question.isPresent()) {
				Optional<Quizz> quizz = iQuizzService.getQuizzById(quizzId);
				if(quizz.isPresent())
				{
					quizz.get().addQuestion(question.get());
					iQuizzService.addQuizz(quizz.get());
				}
				return new ResponseEntity<>(HttpStatus.OK);
			}
			throw new ResourceNotFoundException("Bad Request !");
		} catch (Exception e) {
			System.out.print(e);
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
	}
	@PutMapping("/quizz/{quizzId}/questions")
	public ResponseEntity<?> addQuestionListToQuizz(@PathVariable(value = "quizzId") Long quizzId, @RequestBody Set<Long> questionIds)
	{
		try {
			if (questionIds != null && !questionIds.isEmpty()) {
				Optional<Quizz> quizz = iQuizzService.getQuizzById(quizzId);
				if(quizz.isPresent())
				{
					for(Long questionId : questionIds)
					{
						Question question = iQuestionService.findQuestionById(questionId).get();
						quizz.get().addQuestion(question);
					}
					iQuizzService.addQuizz(quizz.get());
				}
				return new ResponseEntity<>(HttpStatus.OK);
			}
			throw new ResourceNotFoundException("Bad Request !");
		} catch (Exception e) {
			System.out.print(e);
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
	}
	
	@GetMapping("/quizz/stats/{quizzId}")
	public ResponseEntity<QuizzStatsDTO> getQuizzStatsById(@PathVariable(value = "quizzId") Long quizzId){
		try
		{
			/*List<Course> scores = iCourseService.findByQuizzId(quizzId);
			if(scores.isEmpty())
			{
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}
			Double moyenne = 0.;
			for (Course course: scores ) {
				moyenne += course.getScore();
		      }
			moyenne = moyenne/scores.size();
			System.out.print("Here goes average:" + moyenne);*/
			Optional<Quizz> quizz = iQuizzService.getQuizzById(quizzId);
			if(quizz.isPresent()) {
				QuizzStatsDTO result = new QuizzStatsDTO(iUserService.getConnectedUser(), quizz.get());
				return new ResponseEntity<>(result, HttpStatus.OK);
			}
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		catch(Exception e) {
			System.out.print(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
