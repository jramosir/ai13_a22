package utc.fr.AI13_Projet.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import utc.fr.AI13_Projet.security.jwt.JwtUtils;
import utc.fr.AI13_Projet.security.services.UserDetailsImpl;
import utc.fr.AI13_Projet.dto.UserDTO;
import utc.fr.AI13_Projet.dto.UserLoginDTO;
import utc.fr.AI13_Projet.dto.UserRegistrationDTO;
import utc.fr.AI13_Projet.models.User;
import utc.fr.AI13_Projet.service.IUserService;
import utc.fr.AI13_Projet.dao.UserRepository;

@RestController
@RequestMapping("/api/auth/")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	IUserService iUserService;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping("signin")
	public ResponseEntity<?> authenticateUser(@RequestBody UserLoginDTO loginRequest) {
		/*
		 * Receives a userloginDTO and Validates the request with the spring authentication manager
		 * Returns JWT cookie and UserDTO
		 */
		Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getMail(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

		ResponseCookie jwtCookie = jwtUtils.generateJwtCookie(userDetails);

		UserDTO userDTO = new UserDTO();
		userDTO = userDTO.convertToUserDTO(userDetails.getApplicationUser());

		return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, jwtCookie.toString())
				.body(userDTO);
	}

	@PostMapping("signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody UserRegistrationDTO signUpRequest) {
		if (iUserService.existsByMail(signUpRequest.getMail())) {
			return ResponseEntity.badRequest().body("Error: Mail is already in use!");
		}
		//encodding password with bcrypt
		signUpRequest.setPassword(encoder.encode(signUpRequest.getPassword()));
		// Create new user's account
		iUserService.addUser(signUpRequest.convertToEntity());
        return new ResponseEntity(HttpStatus.CREATED);
	}

	@PostMapping("signout")
	public ResponseEntity<?> logoutUser() {
		ResponseCookie cookie = jwtUtils.getCleanJwtCookie();
		return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, cookie.toString())
				.body(HttpStatus.OK);
	}
}
