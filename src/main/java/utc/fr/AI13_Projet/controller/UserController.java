package utc.fr.AI13_Projet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;

import utc.fr.AI13_Projet.dto.UserDTO;
import utc.fr.AI13_Projet.dto.UserRegistrationDTO;
import utc.fr.AI13_Projet.models.Quizz;
import utc.fr.AI13_Projet.models.User;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import utc.fr.AI13_Projet.service.IQuizzService;
import utc.fr.AI13_Projet.service.IUserService;
import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/")
public class UserController {
	@Autowired
	PasswordEncoder encoder;
	private final IUserService iUserService;

	public UserController(IUserService iUserService) {
		this.iUserService = iUserService;
	}

	@GetMapping("user")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<List<UserDTO>> getAllUsers() {
		try {
			//TODO: modifier pour envoyer list de UserDTO
			List<User> userList = iUserService.getAllUsers();
			if (userList.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			List<UserDTO> userDTOList = new ArrayList<UserDTO>();
			for (User user: userList ) {
				UserDTO userD = new UserDTO();
				userD = userD.convertToUserDTO(user);
		        userDTOList.add(userD);
		      }
			return new ResponseEntity<>(userDTOList, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("user/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<UserDTO> getUserById(@PathVariable(value = "id") long id) {
		Optional<User> user = iUserService.getUserById(id);
		UserDTO userDTO = new UserDTO();
		userDTO = userDTO.convertToUserDTO(user);

		if(user != null && user.isPresent()) {
			return ResponseEntity.ok().body(userDTO);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping("myuser")
	public ResponseEntity<UserDTO> getThisUser() {
		try {
			User user = iUserService.getConnectedUser();
			UserDTO userDTO = new UserDTO();
			userDTO = userDTO.convertToUserDTO(user);
			return ResponseEntity.ok().body(userDTO);
		} catch(Exception e) {
			System.out.print(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("user")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<?> addUser(@Validated @RequestBody UserRegistrationDTO user) {
		try {
			if (iUserService.existsByMail(user.getMail())) {
				return ResponseEntity.badRequest().body("Error: Mail is already in use!");
			}
			iUserService.addUser(user.convertToEntity());
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.print(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
    @DeleteMapping("user/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity deleteUserById(@PathVariable(value = "id") long id) {
        Optional<User> user = iUserService.getUserById(id);
        if(user != null && user.isPresent()) {
            iUserService.deleteUser(user.get());
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

	@PutMapping("/user/{id}")
	public ResponseEntity<UserDTO> updateTutorial(@PathVariable("id") long id, @RequestBody User user) {
		Optional<User> userData = iUserService.getUserById(id);
		if (userData.isPresent()) {
			User updatedUser = userData.get();
			//encodding password with bcrypt
			updatedUser.setPassword(encoder.encode(updatedUser.getPassword()));
			iUserService.updateUser(updatedUser);
			return new ResponseEntity<>(HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}