package utc.fr.AI13_Projet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import utc.fr.AI13_Projet.models.Answer;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import utc.fr.AI13_Projet.models.Question;
import utc.fr.AI13_Projet.models.Quizz;
import utc.fr.AI13_Projet.models.User;
import utc.fr.AI13_Projet.service.IAnswerService;
import utc.fr.AI13_Projet.service.IQuestionService;
import utc.fr.AI13_Projet.service.IUserService;


import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/")
public class AnswerController {

    private final IAnswerService iAnswerService;

    @Autowired
    IQuestionService iQuestionService;
    public AnswerController(IAnswerService iAnswerService) {
        this.iAnswerService = iAnswerService;
    }


    @GetMapping("answers")
    public List<Answer> findAllAnswers() {
        return iAnswerService.findAllAnswers();
    }

    @GetMapping("/answer/{id}")
    public ResponseEntity<Answer> findAnswerbyId(@PathVariable(value = "id") long id) {
        Optional<Answer> answer = iAnswerService.findAnswerbyId(id);

        if(answer.isPresent()) {
            return ResponseEntity.ok().body(answer.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @PostMapping("/answer/{answerId}/question/{questionId}")
    public ResponseEntity<?> defineCorrectAnswer(@PathVariable(value = "answerId") Long answerId, @PathVariable(value = "questionId") Long questionId) {
        try {
            Optional<Question> question = iQuestionService.findQuestionById(questionId);
            if (question != null && question.isPresent()) {
                Optional<Answer> answer = iAnswerService.findAnswerById(answerId);
                if (answer != null && answer.isPresent()) {
                    question.get().setCorrectAnswer(answer.get());
                    iQuestionService.updateQuestion(question.get());
                    return new ResponseEntity<>(HttpStatus.OK);
                }
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            System.out.print(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @PostMapping("/answer")
    public Answer addAnswer(@Validated @RequestBody Answer ans) {
        return iAnswerService.saveAnswer(ans);
    }
}