package utc.fr.AI13_Projet.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.modelmapper.ModelMapper;

import utc.fr.AI13_Projet.models.Question;
import utc.fr.AI13_Projet.models.Quizz;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;

public class QuizzDTO {
    private ModelMapper modelMapper;
    private String sujet;
    private Boolean isActive;
    private LocalDateTime creationDate;


    public void setQuizzQuestions(Collection<Question> quizzQuestions) {
        this.quizzQuestions = quizzQuestions;
        if(quizzQuestions == null || quizzQuestions.isEmpty())
            this.questionCount = Long.valueOf(0);
        else
            this.questionCount = quizzQuestions.stream().count();
    }

    public Collection<Question> getQuizzQuestions() {
        return quizzQuestions;
    }
    @JsonIgnore
    private Collection<Question> quizzQuestions;

    public Long getQuestionCount() {
        return questionCount;
    }

    public void setQuestionCount(Long questionCount) {
        this.questionCount = questionCount;
    }

    private Long questionCount;

    public QuizzDTO(ModelMapper modelMapper, String sujet, Boolean isActive, LocalDateTime creationDate, Collection<Question> quizzQuestions) {
        this.modelMapper = new ModelMapper();
        this.setSujet(sujet);
        this.setIsActive(isActive);
        this.setCreationDate(creationDate);
        this.setQuizzQuestions(quizzQuestions);
    }

    public QuizzDTO() {
        modelMapper = new ModelMapper();
    }

    public String getSujet() {
		return sujet;
	}

	public void setSujet(String sujet) {
		this.sujet = sujet;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public Quizz convertToEntity(){
        return modelMapper.map(this, Quizz.class);
    }

    public QuizzDTO convertToUserDTO(Quizz quizz){
        return modelMapper.map(quizz, QuizzDTO.class);

    }
    public QuizzDTO convertToUserDTO(Optional<Quizz> quizz){
        return modelMapper.map(quizz, QuizzDTO.class);

    }
}
