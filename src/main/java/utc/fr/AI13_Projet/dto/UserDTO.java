package utc.fr.AI13_Projet.dto;

import org.modelmapper.ModelMapper;
import utc.fr.AI13_Projet.models.User;

import java.util.Optional;

public class UserDTO {
    private ModelMapper modelMapper;
    private String firstName;
    private String lastName;
    private String mail;
    private String gender;
    private String company;
    private String telephone;

    public UserDTO(ModelMapper modelMapper, String firstName, String lastName, String mail, String gender, String company, String telephone) {
        this.modelMapper = new ModelMapper();
        this.firstName = firstName;
        this.lastName = lastName;
        this.mail = mail;
        this.gender = gender;
        this.company = company;
        this.telephone = telephone;
    }

    public UserDTO() {
        modelMapper = new ModelMapper();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMail() {
        return mail;
    }

    public String getGender() {
        return gender;
    }

    public String getCompany() {
        return company;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public User convertToEntity(){
        return modelMapper.map(this, User.class);
    }

    public UserDTO convertToUserDTO(User user){
        return modelMapper.map(user, UserDTO.class);

    }
    public UserDTO convertToUserDTO(Optional<User> user){
        return modelMapper.map(user, UserDTO.class);

    }
}
