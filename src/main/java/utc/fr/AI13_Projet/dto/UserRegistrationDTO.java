package utc.fr.AI13_Projet.dto;

import org.modelmapper.ModelMapper;
import utc.fr.AI13_Projet.models.User;

public class UserRegistrationDTO {
    private ModelMapper modelMapper;

    private String firstName;
    private String lastName;
    private String mail;
    private String password;
    private String gender;
    private String company;
    private String telephone;

    public UserRegistrationDTO(String firstName, String lastName, String mail, String password, String gender, String company, String telephone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.mail = mail;
        this.password = password;
        this.gender = gender;
        this.company = company;
        this.telephone = telephone;
    }

    public UserRegistrationDTO() {
        this.modelMapper = new ModelMapper();
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMail() {
        return mail;
    }

    public String getPassword() {
        return password;
    }

    public String getGender() {
        return gender;
    }

    public String getCompany() {
        return company;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public User convertToEntity(){
        return modelMapper.map(this, User.class);
    }

    public UserRegistrationDTO convertToUserDTO(User user){
        return modelMapper.map(user, UserRegistrationDTO.class);

    }
}
