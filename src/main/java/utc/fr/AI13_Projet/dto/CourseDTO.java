package utc.fr.AI13_Projet.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.modelmapper.ModelMapper;
import org.springframework.ui.Model;
import utc.fr.AI13_Projet.models.Answer;
import utc.fr.AI13_Projet.models.Course;
import utc.fr.AI13_Projet.models.Quizz;
import utc.fr.AI13_Projet.models.User;

import java.util.Collection;
import java.util.HashMap;
import java.util.Optional;

public class CourseDTO {
    private Collection<Answer> answers;
    private int score;

    public Collection<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Collection<Answer> answers) {
        this.answers = answers;
        this.answerRightMap = new HashMap<>();
        for(Answer answer : this.answers)
        {
            if(answer.getQuestion().getCorrectAnswer().getAnswerId() == answer.getAnswerId())
                answerRightMap.put(answer.getQuestion().getId(), true);
            else
                answerRightMap.put(answer.getQuestion().getId(), false);
        }
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Quizz getQuizz() {
        return quizz;
    }

    public void setQuizz(Quizz quizz) {
        this.quizz = quizz;
    }

    public HashMap<Long, Boolean> getAnswerRightMap() {
        return answerRightMap;
    }

    public void setAnswerRightMap(HashMap<Long, Boolean> answerRightMap) {
        this.answerRightMap = answerRightMap;
    }

    public ModelMapper getModelMapper() {
        return modelMapper;
    }

    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    private int duration;
    private Quizz quizz;
    private HashMap<Long, Boolean> answerRightMap;
    @JsonIgnore
    private ModelMapper modelMapper;

    public CourseDTO(Collection<Answer> answers, int score, int duration, Quizz quizz, ModelMapper modelMapper) {
        this.answers = answers;
        this.score = score;
        this.duration = duration;
        this.quizz = quizz;
        this.modelMapper = new ModelMapper();


    }

    public CourseDTO()
    {
        this.modelMapper = new ModelMapper();
    }
    public Course convertToEntity(){
        return modelMapper.map(this, Course.class);
    }

    public CourseDTO convertToCourseDTO(Course course){
        return modelMapper.map(course, CourseDTO.class);

    }
    public CourseDTO convertToCourseDTO(Optional<Course> course){
        return modelMapper.map(course, CourseDTO.class);

    }

}
