package utc.fr.AI13_Projet.dto;

import org.modelmapper.ModelMapper;

import utc.fr.AI13_Projet.models.Quizz;
import utc.fr.AI13_Projet.models.User;

import java.util.Optional;

public class QuizzCreateDTO {
    private ModelMapper modelMapper;
    private String sujet;

    public QuizzCreateDTO(ModelMapper modelMapper, String sujet) {
        this.modelMapper = new ModelMapper();
        this.setSujet(sujet);
    }

    public QuizzCreateDTO() {
        modelMapper = new ModelMapper();
    }

    public String getSujet() {
		return sujet;
	}

	public void setSujet(String sujet) {
		this.sujet = sujet;
	}

	public Quizz convertToEntity(){
        return modelMapper.map(this, Quizz.class);
    }

    public QuizzCreateDTO convertToQuizzDTO(Quizz quizz){
        return modelMapper.map(quizz, QuizzCreateDTO.class);

    }
    public QuizzCreateDTO convertToQuizzDTO(Optional<Quizz> quizz){
        return modelMapper.map(quizz, QuizzCreateDTO.class);

    }
}
