package utc.fr.AI13_Projet.dto;

import org.modelmapper.ModelMapper;
import utc.fr.AI13_Projet.models.User;
public class UserLoginDTO {
    private String mail;
    private String password;
    ModelMapper modelMapper;

    public UserLoginDTO() {
        modelMapper = new ModelMapper();
    }

    public UserLoginDTO(String mail, String password) {
        this.mail = mail;
        this.password = password;
        this.modelMapper = new ModelMapper();
    }

    public String getMail() {
        return mail;
    }

    public String getPassword() {
        return password;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User convertToEntity(){
        return modelMapper.map(this, User.class);
    }

    public UserLoginDTO convertToUserDTO(User user){
        return modelMapper.map(user, UserLoginDTO.class);

    }
}
