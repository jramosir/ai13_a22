package utc.fr.AI13_Projet.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.modelmapper.ModelMapper;
import utc.fr.AI13_Projet.models.Answer;
import utc.fr.AI13_Projet.models.Course;
import utc.fr.AI13_Projet.models.Quizz;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Optional;

public class CourseSimpleDTO {

    private int score;
    private int totalScore;

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }

    public LocalDateTime getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(LocalDateTime creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    private LocalDateTime creationTimestamp;

    public Long getQuizzId() {
        return quizzId;
    }

    public void setQuizzId(Long quizzId) {
        this.quizzId = quizzId;
    }

    private Long quizzId;

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Quizz getQuizz() {
        return quizz;
    }

    public void setQuizz(Quizz quizz) {
        this.quizz = quizz;
        this.quizzId  = this.quizz.getId();
        this.sujet = quizz.getSujet();
        this.totalScore = quizz.getQuizzQuestions().size();
    }

    public ModelMapper getModelMapper() {
        return modelMapper;
    }

    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    private int duration;

    public String getSujet() {
        return sujet;
    }

    public void setSujet(String sujet) {
        this.sujet = sujet;
    }

    private String sujet;
    @JsonIgnore
    private Quizz quizz;
    @JsonIgnore
    private ModelMapper modelMapper;

    public CourseSimpleDTO(int score, int duration, Quizz quizz, ModelMapper modelMapper, LocalDateTime creationTimestamp) {
        this.score = score;
        this.duration = duration;
        this.quizz = quizz;
        this.creationTimestamp = creationTimestamp;
        this.modelMapper = new ModelMapper();


    }

    public CourseSimpleDTO()
    {
        this.modelMapper = new ModelMapper();
    }
    public Course convertToEntity(){
        return modelMapper.map(this, Course.class);
    }

    public CourseSimpleDTO convertToCourseDTO(Course course){
        return modelMapper.map(course, CourseSimpleDTO.class);

    }
    public CourseSimpleDTO convertToCourseDTO(Optional<Course> course){
        return modelMapper.map(course, CourseSimpleDTO.class);

    }
}
