package utc.fr.AI13_Projet.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import utc.fr.AI13_Projet.models.Course;
import utc.fr.AI13_Projet.models.Quizz;
import utc.fr.AI13_Projet.models.User;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class QuizzStatsDTO {

    private float average;

    public float getAverage() {
        return average;
    }

    public void setAverage(float average) {
        this.average = average;
    }

    public long getMinGrade() {
        return minGrade;
    }

    public void setMinGrade(long minGrade) {
        this.minGrade = minGrade;
    }

    public long getMaxGrade() {
        return maxGrade;
    }

    public void setMaxGrade(long maxGrade) {
        this.maxGrade = maxGrade;
    }
    private long minGrade;
    private long maxGrade;

    public int getAttemptsCount() {
        return attemptsCount;
    }

    public void setAttemptsCount(int attemptsCount) {
        this.attemptsCount = attemptsCount;
    }

    private int attemptsCount;

    public QuizzStatsDTO(User user, Quizz quizz){
        List<Course> courseStream =  user.getCourseDoneList().stream().filter((course) -> course.getQuizz().getId() == quizz.getId()).collect(Collectors.toList());
        Optional<Course> firstCourse = courseStream.stream().findFirst();
        if(firstCourse.isPresent())
        {
            minGrade = firstCourse.get().getScore();
            attemptsCount = courseStream.size();
            int sum = 0;
            for(Course course : courseStream)
            {
                if(course.getScore() > maxGrade)
                    maxGrade = course.getScore();
                else if(course.getScore() < minGrade)
                    minGrade = course.getScore();
                sum += course.getScore();
            }
            average = (float)sum / (float)attemptsCount;
        }

    }
}
